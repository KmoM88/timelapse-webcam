# Webcam Timelapse Project

Este proyecto crea un timelapse automático utilizando una webcam, capturando imágenes a intervalos regulares y generando un video timelapse periódicamente.

## Componentes principales

1. **Captura de imágenes**: Utiliza OpenCV para capturar imágenes de la webcam a intervalos regulares.
2. **Generación de timelapse**: Crea un video timelapse a partir de las imágenes capturadas utilizando FFmpeg.
3. **Limpieza automática**: Gestiona el almacenamiento eliminando imágenes y videos antiguos.
4. **Programación de tareas**: Utiliza un planificador para ejecutar tareas periódicamente.

## Estructura del proyecto

- `src/webcam.py`: Contiene las funciones principales para capturar imágenes, crear timelapses y limpiar directorios.
- `src/scheduler.py`: Implementa la programación de tareas utilizando el módulo `sched` de Python.
- `Dockerfile/Dockerfile.webcam`: Dockerfile para construir la imagen del contenedor principal.
- `Dockerfile/Dockerfile.http`: Dockerfile para construir una imagen de servidor HTTP (para visualización, si es necesario).

## Requisitos

- Docker
- Webcam compatible

## Uso

1. Construir la imagen Docker:
   ```
   docker build -t webcam-timelapse -f Dockerfile/Dockerfile.webcam .
   ```

2. Ejecutar el contenedor:
   ```
   docker run -it --device=/dev/video1:/dev/video0 -v=$PWD/output:/app/output webcam-timelapse python scheduler.py
   ```

   Nota: Asegúrate de que `/dev/video1` sea el dispositivo correcto para tu webcam. Ajusta según sea necesario.

## Configuración

Puedes ajustar los siguientes parámetros en `src/webcam.py` y `src/scheduler.py`:

- `output_dir`: Directorio de salida para imágenes y videos.
- `max_files`: Número máximo de archivos de imagen a mantener.
- `max_files_vid`: Número máximo de archivos de video a mantener.
- `framerate`: Tasa de frames para el video timelapse.
- `capture_period`: Intervalo entre capturas de imagen (en segundos).
- `clean_period`: Intervalo para la limpieza de directorios (en segundos).
- `video_period`: Intervalo para la creación de videos timelapse (en segundos).

## Notas adicionales

- Las imágenes se rotan 180 grados antes de guardarse.
- El proyecto incluye un decorador para medir el tiempo de ejecución de las funciones principales.
- Se proporciona un Dockerfile adicional para servir el timelapse a través de un servidor HTTP si es necesario.

## Requisitos de Python

Los requisitos de Python se encuentran en `src/requirements.txt`. Las principales dependencias son:
- numpy
- opencv-python
