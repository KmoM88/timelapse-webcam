import sched
import time
import webcam

scheduler = sched.scheduler(time.time, time.sleep)
clean_period = 60*60
video_period = 60*60*24
capture_period = 1

def capture_image():
    print('Taking image')
    webcam.capture_image()
    scheduler.enter(capture_period, 1, capture_image, ())

def clean_dir():
    print('Clean dir')
    webcam.clean_dir()
    scheduler.enter(clean_period, 2, clean_dir, ())

def create_timelapse():
    print('Create timalapse')
    webcam.create_timelapse()
    scheduler.enter(video_period, 2, create_timelapse, ())


scheduler.enter(capture_period, 1, capture_image, ())
scheduler.enter(0.5, 2, clean_dir, ())
scheduler.enter(1, 2, create_timelapse, ())

scheduler.run()
