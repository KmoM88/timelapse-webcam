import cv2
import os
import time
import glob
import subprocess

import numpy as np

from functools import wraps

output_dir = "./output"
os.makedirs(output_dir, exist_ok=True)
os.makedirs(f'{output_dir}/images', exist_ok=True)
max_files = 25*60*60*24
max_files_vid = 24*20
output_file = "timelapse"
framerate = 25

def measure_time(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        elapsed_time = end_time - start_time
        print(f"Time taken for {func.__name__}: {elapsed_time:.4f} seconds")
        return result
    return wrapper

@measure_time
def capture_image():
    cap = cv2.VideoCapture(0)
    ret, frame = cap.read()
    if ret:
        frame = cv2.rotate(frame, cv2.ROTATE_180)
        timestamp = int(time.time())
        image_path = os.path.join(f'{output_dir}/images', f'image_{timestamp}.jpg')
        cv2.imwrite(image_path, frame)
        print(f"Image captured: {image_path}")
        cap.release()
    else:
        print("Failed to capture image from webcam.")

@measure_time
def clean_dir():
    jpg_files = glob.glob(os.path.join(f'{output_dir}/images/', '*.jpg'))
    mp4_files = glob.glob(os.path.join(f'{output_dir}/', '*.mp4'))
    
    if len(jpg_files) > max_files:
        jpg_files.sort(key=os.path.getctime)
        num_files_to_delete = len(jpg_files) - max_files
        for i in range(num_files_to_delete):
            os.remove(jpg_files[i])
            print("Removed old photo:", jpg_files[i])
    
    if len(mp4_files) > max_files_vid:
        mp4_files.sort(key=os.path.getctime)
        num_files_to_delete = len(mp4_files) - max_files_vid
        for i in range(num_files_to_delete):
            os.remove(mp4_files[i])
            print("Removed video:", mp4_files[i])

@measure_time
def create_timelapse():
    unique_frames = sorted(glob.glob(os.path.join(f'{output_dir}/images', '*.jpg')))
    timestamp = int(time.time())
    existing_file = os.path.join(output_dir, output_file)

    if not os.path.isdir(output_dir):
        print(f"Error: Output directory '{output_dir}' does not exist")
        return

    if os.path.exists(f'{existing_file}.mp4'):
        os.remove(f'{existing_file}.mp4')
        print(f"Removed existing timelapse file: {existing_file}.mp4")

    with open("frame_list.txt", "w") as f:
        for frame in unique_frames:
            f.write(f"file '{frame}'\n")

    try:
        subprocess.run([
            'ffmpeg', '-f', 'concat', '-safe', '0', '-i', 'frame_list.txt', 
            '-c:v', 'libx264', '-r', str(framerate), '-pix_fmt', 'yuv420p',
            f'{existing_file}_{timestamp}.mp4'
        ], check=True)
        print("Timelapse created successfully:", f'{existing_file}_{timestamp}.mp4')
    except subprocess.CalledProcessError as e:
        print("Error creating timelapse:", e)

    os.remove("frame_list.txt")